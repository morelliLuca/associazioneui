package view;

import javax.swing.*;
import java.awt.*;

public class GraphicsComponentFactory {
    private Color backgroudColor;
    private Color foregroudColor;
    private Font font;

    public GraphicsComponentFactory(Color backgroudColor, Color foregroudColor, Font font){
        this.backgroudColor=backgroudColor;
        this.foregroudColor=foregroudColor;
        this.font=font;
    }

    public JButton createOptionButton(String text){
        JButton jButton = new JButton(text);
        jButton.setBackground(backgroudColor);
        jButton.setForeground(foregroudColor);
        jButton.setFont(font);
        return jButton;
    }
}
