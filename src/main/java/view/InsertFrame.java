package view;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.plaf.PanelUI;
import java.awt.*;
import java.util.Locale;

public class InsertFrame extends JFrame {
    GraphicsComponentFactory graphicsComponentFactory = new GraphicsComponentFactory(Color.BLUE, Color.RED, new Font("MyFont",Font.BOLD,25));

    public InsertFrame(){
        super("Inserisci Nuovo Utente");
        setSize(1000,800);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //TODO: change in return to HomeFrame
        Container c=this.getContentPane();

        c.setLayout(new BorderLayout());

        JPanel titlePanel = createTitlePanel();
        c.add(titlePanel,BorderLayout.NORTH);

        JPanel jPanel = createInsertionFieldsPanel();
        c.add(jPanel,BorderLayout.CENTER);

        JPanel insertionButtonsPanel = createInsertionButtonsPanel();
        c.add(insertionButtonsPanel,BorderLayout.SOUTH);
    }

    public static JPanel createInsertionFieldsPanel() {
        JPanel insertionPanel=new JPanel();

        JTextField cardNumberTextField = new JTextField();
        cardNumberTextField.setUI(new JTextFieldHintUI("Numero Tessera", Color.GRAY));

        JTextField nameTextField = new JTextField();
        nameTextField.setUI(new JTextFieldHintUI("Nome",Color.GRAY));

        JTextField surnameTextField = new JTextField();
        surnameTextField.setUI(new JTextFieldHintUI("Cognome",Color.GRAY));

        JRadioButton maleRadioButton = new JRadioButton("M");
        JRadioButton femaleRadioButton = new JRadioButton("F");
        ButtonGroup sexButtons = new ButtonGroup();
        sexButtons.add(maleRadioButton);
        sexButtons.add(femaleRadioButton);

        JDateChooser chooser = new JDateChooser();
        chooser.setLocale(Locale.ITALY);

        JTextField emailTextField = new JTextField();
        emailTextField.setUI(new JTextFieldHintUI("Email",Color.GRAY));

        JTextField numberTextField = new JTextField();
        numberTextField.setUI(new JTextFieldHintUI("Telefono",Color.GRAY));

        insertionPanel.add(cardNumberTextField);
        insertionPanel.add(nameTextField);
        insertionPanel.add(surnameTextField);
        insertionPanel.add(maleRadioButton);
        insertionPanel.add(chooser);
        insertionPanel.add(emailTextField);
        insertionPanel.add(numberTextField);

        GroupLayout layout = new GroupLayout(insertionPanel);
        insertionPanel.setLayout(layout);

        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        GroupLayout.SequentialGroup horizontalGroup = layout.createSequentialGroup();
        horizontalGroup.addGroup(layout.createParallelGroup()
            .addComponent(cardNumberTextField)
            .addComponent(nameTextField)
            .addComponent(surnameTextField)
            .addComponent(maleRadioButton)
            .addComponent(chooser)
            .addComponent(emailTextField)
            .addComponent(numberTextField)
        );
        horizontalGroup.addGroup(layout.createParallelGroup().addComponent(femaleRadioButton));
        layout.setHorizontalGroup(horizontalGroup);

        GroupLayout.SequentialGroup verticalGroup = layout.createSequentialGroup();
        verticalGroup.addGroup(layout.createParallelGroup().addComponent(cardNumberTextField));
        verticalGroup.addGroup(layout.createParallelGroup().addComponent(nameTextField));
        verticalGroup.addGroup(layout.createParallelGroup().addComponent(surnameTextField));
        verticalGroup.addGroup(layout.createParallelGroup()
            .addComponent(maleRadioButton)
            .addComponent(femaleRadioButton)
        );
        verticalGroup.addGroup(layout.createParallelGroup().addComponent(chooser));
        verticalGroup.addGroup(layout.createParallelGroup().addComponent(emailTextField));
        verticalGroup.addGroup(layout.createParallelGroup().addComponent(numberTextField));
        layout.setVerticalGroup(verticalGroup);

        return insertionPanel;
    }

    private JPanel createInsertionButtonsPanel() {
        JPanel insertionButtonsPanel = new JPanel();
        JButton insertButton = graphicsComponentFactory.createOptionButton("Inserisci");
        JButton backButton = graphicsComponentFactory.createOptionButton("Annulla");
        insertionButtonsPanel.setLayout(new FlowLayout());
        insertionButtonsPanel.add(insertButton);
        insertionButtonsPanel.add(backButton);
        return insertionButtonsPanel;
    }

    private JPanel createTitlePanel() {
        JPanel titlePanel = new JPanel();
        titlePanel.setLayout(new FlowLayout());
        JLabel titleLabel = new JLabel("NUOVO UTENTE");
        titlePanel.add(titleLabel);
        return titlePanel;
    }
}
