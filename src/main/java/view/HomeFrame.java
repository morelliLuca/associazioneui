package view;

import javax.swing.*;
import java.awt.*;

public class HomeFrame extends JFrame{
    JLabel titleLable = new JLabel("home associazione");

    JPanel homePanel = new JPanel(new BorderLayout());

    JPanel optionButtonPanel = new JPanel(new FlowLayout());

    GraphicsComponentFactory graphicsComponentFactory = new GraphicsComponentFactory(Color.BLUE, Color.RED, new Font("MyFont",Font.BOLD,25));
    
    JButton newUserButton = graphicsComponentFactory.createOptionButton("Nuovo Utente");

    JButton findUserButton = graphicsComponentFactory.createOptionButton("Cerca Utente");
    JButton printUsersButton = graphicsComponentFactory.createOptionButton("Stampa Elenco");

    JScrollPane scrollPane = new JScrollPane();

    public HomeFrame(){
        super("Gestione Utenti");
        setSize(1000,800);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container c=this.getContentPane();
        c.setLayout(new BorderLayout());

        JPanel titlePanel = new JPanel();
        titlePanel.add(titleLable);
        c.add(titlePanel,BorderLayout.NORTH);

        createOptionsBuildPanel();
        c.add(optionButtonPanel,BorderLayout.CENTER);

        JTable tabellaUtenti = createTable();
        c.add(new JScrollPane(tabellaUtenti),BorderLayout.SOUTH);
    }

    private void createOptionsBuildPanel() {
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER, 50, 10);
        optionButtonPanel.setLayout(flowLayout);
        optionButtonPanel.add(newUserButton);
        optionButtonPanel.add(findUserButton);
        optionButtonPanel.add(printUsersButton);
    }

    private JTable createTable(){
	/*Test Table*/
        Object[][] data = new Object[][]{
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"},
                {1,"Rossi", "Marco","1/1/1990","marco.rossi@gmail.com","3382834459"},
                {2,"Bianchi", "Luca","1/1/1980","luca.bianchi@gmail.com","0245100359"}
        };
        String[] columnName = new String[] {"NUMERO TESSERA","COGNOME","NOME","DATA NASCITA","MAIL","NUMERO"};
        return new JTable(data,columnName);
    }
}
